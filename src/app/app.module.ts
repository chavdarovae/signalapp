import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularMaterialModule } from './angular-material/angular-material.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ChangeDetectionComponent } from './change-detection/change-detection.component';
import { ComparisonComponent } from './comparison/comparison.component';
import { EquationsComponent } from './comparison/equations/equations.component';
import { RxjsComponent } from './comparison/rxjs/rxjs.component';
import { SignalsComponent } from './comparison/signals/signals.component';
import { VariablesComponent } from './comparison/variables/variables.component';
import { CoreModule } from './core/core.module';
import { SignalsConceptComponent } from './signals-concept/signals-concept.component';
import { OnPushComponent } from './change-detection/on-push/on-push.component';
import { DefaultComponent } from './change-detection/default/default.component';
import { HomeComponent } from './home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    ComparisonComponent,
    SignalsConceptComponent,
    ChangeDetectionComponent,
    VariablesComponent,
    RxjsComponent,
    EquationsComponent,
	SignalsComponent,
 OnPushComponent,
 DefaultComponent,
 HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AngularMaterialModule,
    CoreModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
