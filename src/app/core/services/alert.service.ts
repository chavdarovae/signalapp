import { Injectable, WritableSignal, computed, signal } from '@angular/core';

@Injectable({
	providedIn: 'root'
})
export class AlertService {
	private alertSignal: WritableSignal<string[]> = signal<string[]>([]);
	alert = computed(() => this.alertSignal);
	alertType!: 'danger' | 'warning' | 'info' | 'success';

	showAlert(alerts: string[], type: 'danger' | 'warning' | 'info' | 'success') {
		this.clearAlerts();
		this.alertType = type;
		this.alertSignal.set(alerts);
	}

	clearAlerts() {
		this.alertSignal.set([]);
	}

	showUpdateAlert() {
		this.showAlert([ ('Alfa ist gleich Beta und die Summe ist aktualisiert.') ], 'success')
	}

}
