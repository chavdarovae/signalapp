import { ChangeDetectionStrategy, Component, Input, ViewChild, inject } from '@angular/core';
import { MatAccordion } from '@angular/material/expansion';
import { AppService } from 'src/app/app.service';

@Component({
	selector: 'app-default',
	templateUrl: './default.component.html',
	styleUrls: ['./default.component.scss'],
	changeDetection: ChangeDetectionStrategy.Default
})
export class DefaultComponent {
	appService = inject(AppService);

	@Input() title!: string;
	@Input() alfa: number = 0;
	@Input() betaArr: number[] = [];

	@ViewChild(MatAccordion) accordion!: MatAccordion;

	onBetaChanged(event: string) {
		this.betaArr[1] = +event;
		// this.betaArr = [...this.betaArr];
	}

	log(param: 'inp' | 'inj') {
		console.log('Component 1');
		switch (param) {
			case 'inp':
				console.log('Ⲁ = ' + this.alfa);
				console.log(`β = ${[...this.betaArr]}`);
				break;
			case 'inp':
				console.log('couner = ' + this.appService.count);
				console.log(`Δ = ${[...this.appService.deltaArr]}`);
				break;

		}
	}
}
