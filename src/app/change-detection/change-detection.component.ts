import { Component, inject } from '@angular/core';
import { AppService } from '../app.service';

@Component({
  selector: 'app-change-detection',
  templateUrl: './change-detection.component.html',
  styleUrls: ['./change-detection.component.scss']
})
export class ChangeDetectionComponent {
	appService = inject(AppService);

	alfa = 10;
	betaArr = [8,16,24];

	onBetaChanged(event: string) {
		this.betaArr[1] = +event;
		// this.betaArr = [...this.betaArr];
	}
}
