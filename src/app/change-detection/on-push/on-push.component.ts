import { ChangeDetectionStrategy, Component, Input, ViewChild, inject } from '@angular/core';
import { MatAccordion } from '@angular/material/expansion';
import { AppService } from 'src/app/app.service';

@Component({
  selector: 'app-on-push',
  templateUrl: './on-push.component.html',
  styleUrls: ['./on-push.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OnPushComponent {
	appService = inject(AppService);
	// changeDetector = inject(ChangeDetectorRef);

	@Input() title!: string;
	@Input() alfa: number = 0;
	@Input() betaArr: number[] = [];

	@ViewChild(MatAccordion) accordion!: MatAccordion;

	getArr(): number[] {
		return this.appService.deltaArr;
	}

	log(param: 'inp' | 'inj') {
		console.log('Component 2');
		switch (param) {
			case 'inp':
				console.log('Ⲁ = ' + this.alfa);
				console.log(`β = ${[...this.betaArr]}`);
				break;
			case 'inp':
				console.log('couner = ' + this.appService.count);
				console.log(`Δ = ${[...this.appService.deltaArr]}`);
				break;
		}
	}
}
