import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AppService {
	count = 0;
	deltaArr = [12, 13, 14, 15];
	private deltaArrSubject = new BehaviorSubject (this.deltaArr);
	deltaArr$ = this.deltaArrSubject.asObservable();

	pushNewDelta(delta: number): void {
		this.deltaArr.push(delta);
		this.deltaArr = [...this.deltaArr];
		this.deltaArrSubject.next([...this.deltaArr]);
	}

	shiftDelatArr(): void {
		this.deltaArr.shift();
		this.deltaArrSubject.next([...this.deltaArr]);
	}

	toggleFlag(): void {
		this.count++;
	}
}
