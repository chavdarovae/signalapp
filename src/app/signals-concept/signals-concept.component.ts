import { Component, computed, effect, inject, signal } from '@angular/core';
import { AlertService } from './../core/services/alert.service';

interface Person {
	name: string,
	age: number
}

@Component({
	selector: 'app-signals-concept',
	templateUrl: './signals-concept.component.html',
	styleUrls: ['./signals-concept.component.scss']
})
export class SignalsConceptComponent {
	// service injection
	alertService = inject(AlertService);

	// auxiliary template variables
	staffFlag = false;
	staffUnderFlag = false;

	// create Signal
	staffSignal = signal<Person[]>([]);
	staffCountSingnal = signal<number>(0);

	// read Signals
	readSignals() {
		const personArr = this.staffSignal();
		const personCount = this.staffCountSingnal();
	}

	// create Computed
	staffUnder35Computed = computed(() => this.staffSignal().filter((p: Person) => p.age < 35));
	staffUnder35CountComputed = computed(() => this.staffSignal().filter((p: Person) => p.age < 35).length);

	// create Effect
	constructor() {
		effect(() => {
			if (this.staffSignal().length) {
				this.staffFlag = true;
				this.sideEffects();
			}
		}, { allowSignalWrites: true });
		effect(() => {
			if (this.staffUnder35Computed().length) {
				this.staffUnderFlag = true;
				this.sideEffects();
			}
		}, { allowSignalWrites: true });
	}

	// set Signal
	setSignals(): void {
		this.staffSignal.set([{ name: 'John Doe', age: 40 }]);
		this.staffCountSingnal.set(1);
	}

	// upate Signal
	updateSignals() {
		this.staffSignal.mutate(persons => persons.push({ name: 'Jane Doe', age: 33 }));
		this.staffCountSingnal.update(count => count + 1);
	}


	private sideEffects () {
		this.alertService.showAlert(['The staff list has been updated'], 'success');

		setTimeout(() => {
			this.staffFlag = false;
			this.staffUnderFlag = false;
			this.alertService.clearAlerts();
		}, 1000)
	}
}
