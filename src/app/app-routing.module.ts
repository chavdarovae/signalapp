import { RouterModule, Routes } from '@angular/router';
import { ChangeDetectionComponent } from './change-detection/change-detection.component';
import { ComparisonComponent } from './comparison/comparison.component';
import { HomeComponent } from './home/home.component';
import { SignalsConceptComponent } from './signals-concept/signals-concept.component';

const routes: Routes = [
	{
		path: '',
		component: HomeComponent
	},
	{
		path: 'signals',
		component: SignalsConceptComponent
	},
	{
		path: 'chnage-detection',
		component: ChangeDetectionComponent
	},
	{
		path: 'compare',
		component: ComparisonComponent
	}
];

export const AppRoutingModule = RouterModule.forRoot(routes, { anchorScrolling: 'enabled', scrollPositionRestoration: 'disabled', onSameUrlNavigation: 'reload', useHash: true });
