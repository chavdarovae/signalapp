import { ChangeDetectionStrategy, Component, Input, OnInit, inject } from '@angular/core';
import { AlertService } from 'src/app/core/services/alert.service';

@Component({
  selector: 'app-variables',
  templateUrl: './variables.component.html',
  styleUrls: ['./variables.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default
})
export class VariablesComponent implements OnInit {
	alertService = inject(AlertService)
	@Input() betaArr: number[] = [];

	alfa = 5;
	beta = [10, 15, 20];

	ngOnInit(): void {
		// setInterval(() => {
		// 	this.alfa++;
		// 	this.beta[1]++;
		// 	console.log(this.alfa);
		// 	console.log(this.beta);
		// },
		// 1000)
	}

	getSum(): number {
		console.log('Variable sum calculated');
		return +this.alfa + this.beta[1];
	}

	onAlfaInput(value: string) {
		this.alfa = +value;

		if(this.alfa === this.beta[1]) {
			this.alertService.showUpdateAlert();
		} else {
			this.alertService.clearAlerts();
		}
	}

	logVariables() {
		console.log('Print variables:');
	}
}
