 import { ChangeDetectionStrategy, Component, Input, OnInit, inject } from '@angular/core';
import { BehaviorSubject, combineLatest, map, tap } from 'rxjs';
import { AlertService } from 'src/app/core/services/alert.service';

@Component({
  selector: 'app-rxjs',
  templateUrl: './rxjs.component.html',
  styleUrls: ['./rxjs.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default
})
export class RxjsComponent implements OnInit {
	alertService = inject(AlertService);
	@Input() betaArr: number[] = [];

	alfa$ = new BehaviorSubject(6);
	beta$ = new BehaviorSubject([10, 16, 20]);
	sum$ = combineLatest([this.alfa$.asObservable(), this.beta$.asObservable()]).pipe(
		tap((res) => {
			console.log('Subject sum calculated');
			if(res[0] === res[1][1]) {
				this.alertService.showUpdateAlert();
			} else {
				this.alertService.clearAlerts();
			}
		}),
		map((res) => res[0] + res[1][1])
	);

	ngOnInit(): void {
		// setInterval(() => {
		// 	this.alfa$.next(this.alfa$.getValue() + 1);
		// 	this.beta$.next([10, (this.beta$.getValue()[1] + 1), 20]);
		// 	console.log(this.alfa$.getValue());
		// 	console.log(this.beta$.getValue());
		// },
		// 1000)
	}

	logSubjects() {
		console.log('subjects:');
	}
}
