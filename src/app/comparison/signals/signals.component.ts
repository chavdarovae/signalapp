import { ChangeDetectionStrategy, Component, OnInit, computed, effect, inject, signal } from '@angular/core';
import { AlertService } from 'src/app/core/services/alert.service';

@Component({
	selector: 'app-signals',
	templateUrl: './signals.component.html',
	styleUrls: ['./signals.component.scss'],
	changeDetection: ChangeDetectionStrategy.Default
})
export class SignalsComponent implements OnInit {
	alertService = inject(AlertService);

	sAlfa = signal(7);
	sBeta = signal([10, 17, 20]);
	sSum = computed(() => {
		console.log('Signal sum calculated');
		return this.sAlfa() + this.sBeta()[1];
	});

	constructor() {
		effect(() => {
			if (+this.sAlfa() === this.sBeta()[1]) {
				this.alertService.showUpdateAlert();
			} else {
				this.alertService.clearAlerts();
			}

		}, { allowSignalWrites: true})
	}

	ngOnInit(): void {
		// setInterval(() => {
		// 	this.sAlfa.update(alfa => alfa + 1);
		// 	this.sBeta.update(beta => {
		// 		beta[1]++;
		// 		return beta;
		// 	})
		// 	console.log(this.sAlfa());
		// 	console.log(this.sBeta());
		// },
		// 1000)
	}

	logSignals() {
		console.log('Prints Signals:');
	}
}
