import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-equations',
  templateUrl: './equations.component.html',
  styleUrls: ['./equations.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EquationsComponent {
	@Input() type: string = '';
	@Input() alfa: number = 0;
	@Input() beta: number[] = [0, 0, 0];
	@Input() sum: number | null = 0;

	@Output() log: EventEmitter<unknown> = new EventEmitter();

}
